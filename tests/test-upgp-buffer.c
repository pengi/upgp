#include <check.h>

#include <upgp/core/buffer.h>

#include <stdio.h>

/*
 * val - count
 * 0 - 8
 * 1 - 10
 * 2 - 7
 * 3 - 7
 */
uint8_t some_data[32] = {
    0,1,2,3,0,1,2,3,
    1,1,2,3,1,1,2,3,
    0,0,0,0,0,1,2,3,
    1,1,2,3,0,1,2,3
};

START_TEST(test_mpi_counter_size)
{
    UPGP_BUFFER(buffer, 32);

    ck_assert_int_eq(0, upgp_buffer_size(&buffer));
    ck_assert_int_eq(0, upgp_buffer_counter(&buffer));

    ck_assert_int_eq(12, upgp_buffer_append(&buffer, some_data, 12));

    ck_assert_int_eq(12, upgp_buffer_size(&buffer));
    ck_assert_int_eq(0, upgp_buffer_counter(&buffer));

    upgp_buffer_take(&buffer, 4);

    ck_assert_int_eq(8, upgp_buffer_size(&buffer));
    ck_assert_int_eq(4, upgp_buffer_counter(&buffer));

    upgp_buffer_take(&buffer, 3);

    ck_assert_int_eq(5, upgp_buffer_size(&buffer));
    ck_assert_int_eq(7, upgp_buffer_counter(&buffer));

    ck_assert_int_eq(2, upgp_buffer_append(&buffer, some_data, 2));

    ck_assert_int_eq(7, upgp_buffer_size(&buffer));
    ck_assert_int_eq(7, upgp_buffer_counter(&buffer));

    /* Counter shouldn't be cleared just because buffer is empty */
    upgp_buffer_take(&buffer, 7);

    ck_assert_int_eq(0, upgp_buffer_size(&buffer));
    ck_assert_int_eq(14, upgp_buffer_counter(&buffer));

    /* Counter should be cleared when buffer is fully reset */
    upgp_buffer_clear(&buffer);

    ck_assert_int_eq(0, upgp_buffer_size(&buffer));
    ck_assert_int_eq(0, upgp_buffer_counter(&buffer));
}
END_TEST

int main(void)
{
    Suite *s = suite_create("Buffer handling");
    TCase *tc;
    SRunner *sr = srunner_create(s);
    int nf;

    tc = tcase_create("Buffer handling");
    suite_add_tcase(s, tc);
    tcase_add_test(tc, test_mpi_counter_size);

    srunner_run_all(sr, CK_ENV);
    nf = srunner_ntests_failed(sr);
    srunner_free(sr);

    return nf == 0 ? 0 : 1;
}
