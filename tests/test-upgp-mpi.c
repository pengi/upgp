#include <check.h>

#include <upgp/crypto/mpi.h>

#include <stdio.h>

START_TEST(test_mpi_copy_same_size)
{
    upgp_mpi32_t src = {4, {1,2,3,4}};
    upgp_mpi32_t dst = {4, {0,0,0,0}};
    upgp_mpi32_t exp = {4, {1,2,3,4}};

    ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_copy(UPGP_MPI(&dst), UPGP_MPI(&src)));
    ck_assert_int_eq(0, memcmp(&dst, &exp, sizeof(upgp_mpi32_t)));
}
END_TEST

START_TEST(test_mpi_copy_size_increase)
{
    upgp_mpi32_t src = {3, {1,2,3,0}};
    upgp_mpi32_t dst = {4, {0,0,0,0}};
    upgp_mpi32_t exp = {4, {1,2,3,0}};

    ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_copy(UPGP_MPI(&dst), UPGP_MPI(&src)));
    ck_assert_int_eq(0, memcmp(&dst, &exp, sizeof(upgp_mpi32_t)));
}
END_TEST

START_TEST(test_mpi_copy_size_decrease_fit)
{
    upgp_mpi32_t src = {4, {1,2,3,0}};
    upgp_mpi32_t dst = {3, {0,0,0,0}};
    upgp_mpi32_t exp = {3, {1,2,3,0}};

    ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_copy(UPGP_MPI(&dst), UPGP_MPI(&src)));
    ck_assert_int_eq(0, memcmp(&dst, &exp, sizeof(upgp_mpi32_t)));
}
END_TEST

START_TEST(test_mpi_copy_size_decrease_overflow)
{
    upgp_mpi32_t src = {4, {1,2,3,3}};
    upgp_mpi32_t dst = {3, {0,0,0,0}};
    upgp_mpi32_t exp = {3, {1,2,3,0}};

    ck_assert_int_eq(UPGP_MPI_ERROR_OVERFLOW, upgp_mpi_copy(UPGP_MPI(&dst), UPGP_MPI(&src)));
    ck_assert_int_eq(0, memcmp(&dst, &exp, sizeof(upgp_mpi32_t)));
}
END_TEST

START_TEST(test_mpi_copy_uint32_same_size)
{
    uint32_t src = 0x01020304;
    upgp_mpi64_t dst = {4, {0,0,0,0,0,0,0,0}};
    upgp_mpi64_t exp = {4, {4,3,2,1,0,0,0,0}};

    ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_copy_uint32(UPGP_MPI(&dst), src));
    ck_assert_int_eq(0, memcmp(&dst, &exp, sizeof(upgp_mpi64_t)));
}
END_TEST

START_TEST(test_mpi_copy_uint32_size_increase)
{
    uint32_t src = 0x01020304;
    upgp_mpi64_t dst = {5, {0,0,0,0,0,0,0,0}};
    upgp_mpi64_t exp = {5, {4,3,2,1,0,0,0,0}};

    ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_copy_uint32(UPGP_MPI(&dst), src));
    ck_assert_int_eq(0, memcmp(&dst, &exp, sizeof(upgp_mpi64_t)));
}
END_TEST

START_TEST(test_mpi_copy_uint32_size_decrease_fit)
{
    uint32_t src = 0x00020304;
    upgp_mpi64_t dst = {3, {0,0,0,0,0,0,0,0}};
    upgp_mpi64_t exp = {3, {4,3,2,0,0,0,0,0}};

    ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_copy_uint32(UPGP_MPI(&dst), src));
    ck_assert_int_eq(0, memcmp(&dst, &exp, sizeof(upgp_mpi64_t)));
}
END_TEST

START_TEST(test_mpi_copy_uint32_size_decrease_overflow)
{
    uint32_t src = 0x01020304;
    upgp_mpi64_t dst = {3, {0,0,0,0,0,0,0,0}};
    upgp_mpi64_t exp = {3, {4,3,2,0,0,0,0,0}};

    ck_assert_int_eq(UPGP_MPI_ERROR_OVERFLOW, upgp_mpi_copy_uint32(UPGP_MPI(&dst), src));
    ck_assert_int_eq(0, memcmp(&dst, &exp, sizeof(upgp_mpi64_t)));
}
END_TEST

START_TEST(test_mpi_compare_same_size)
{
    upgp_mpi64_t hi = {4, {1,2,3,4,0,0,0,0}};
    upgp_mpi64_t low = {4, {1,1,4,4,0,0,0,0}};

    ck_assert_int_gt(upgp_mpi_compare(UPGP_MPI(&hi), UPGP_MPI(&low)), 0);
    ck_assert_int_eq(upgp_mpi_compare(UPGP_MPI(&hi), UPGP_MPI(&hi)), 0);
    ck_assert_int_lt(upgp_mpi_compare(UPGP_MPI(&low), UPGP_MPI(&hi)), 0);
}
END_TEST

START_TEST(test_mpi_compare_long_hi)
{
    upgp_mpi64_t hi = {5, {1,2,3,4,0,0,0,0}};
    upgp_mpi64_t low = {4, {1,1,4,4,0,0,0,0}};

    ck_assert_int_gt(upgp_mpi_compare(UPGP_MPI(&hi), UPGP_MPI(&low)), 0);
    ck_assert_int_eq(upgp_mpi_compare(UPGP_MPI(&hi), UPGP_MPI(&hi)), 0);
    ck_assert_int_lt(upgp_mpi_compare(UPGP_MPI(&low), UPGP_MPI(&hi)), 0);
}
END_TEST

START_TEST(test_mpi_compare_long_low)
{
    upgp_mpi64_t hi = {4, {1,2,3,4,0,0,0,0}};
    upgp_mpi64_t low = {5, {1,1,4,4,0,0,0,0}};

    ck_assert_int_gt(upgp_mpi_compare(UPGP_MPI(&hi), UPGP_MPI(&low)), 0);
    ck_assert_int_eq(upgp_mpi_compare(UPGP_MPI(&hi), UPGP_MPI(&hi)), 0);
    ck_assert_int_lt(upgp_mpi_compare(UPGP_MPI(&low), UPGP_MPI(&hi)), 0);
}
END_TEST

START_TEST(test_mpi_add_mod_same_size)
{
    upgp_mpi32_t acc = {4, {0,0,0,0}};
    upgp_mpi32_t exp = {4, {0,0,0,0}};
    upgp_mpi32_t term = {4, {0,0,0,0}};
    upgp_mpi32_t mod = {4, {0,0,0,0}};

    upgp_mpi_copy_uint32(UPGP_MPI(&term), 0x78563412);
    upgp_mpi_copy_uint32(UPGP_MPI(&mod), 0x9712ab32);

    upgp_mpi_copy_uint32(UPGP_MPI(&exp), 0x00000000);
    ck_assert_int_eq(upgp_mpi_compare(UPGP_MPI(&acc), UPGP_MPI(&exp)), 0);

    ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_add_mod(UPGP_MPI(&acc), UPGP_MPI(&acc), UPGP_MPI(&term), UPGP_MPI(&mod)));

    upgp_mpi_copy_uint32(UPGP_MPI(&exp), 0x78563412);
    ck_assert_int_eq(upgp_mpi_compare(UPGP_MPI(&acc), UPGP_MPI(&exp)), 0);

    ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_add_mod(UPGP_MPI(&acc), UPGP_MPI(&acc), UPGP_MPI(&term), UPGP_MPI(&mod)));

    upgp_mpi_copy_uint32(UPGP_MPI(&exp), 0x5999bcf2);
    ck_assert_int_eq(upgp_mpi_compare(UPGP_MPI(&acc), UPGP_MPI(&exp)), 0);

    ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_add_mod(UPGP_MPI(&acc), UPGP_MPI(&acc), UPGP_MPI(&term), UPGP_MPI(&mod)));

    upgp_mpi_copy_uint32(UPGP_MPI(&exp), 0x3add45d2);
    ck_assert_int_eq(upgp_mpi_compare(UPGP_MPI(&acc), UPGP_MPI(&exp)), 0);
}
END_TEST

START_TEST(test_mpi_add_mod_different_term_sizes)
{
    upgp_mpi32_t acc = {3, {0,0,0,0}};
    upgp_mpi32_t exp = {4, {0,0,0,0}};
    upgp_mpi32_t term = {2, {0,0,0,0}};
    upgp_mpi32_t mod = {4, {0,0,0,0}};

    uint32_t acc_int = 0x00000000;
    uint32_t term_int = 0x0000f123;
    uint32_t mod_int = 0x00017439;

    int i;

    ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_copy_uint32(UPGP_MPI(&term), term_int));
    ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_copy_uint32(UPGP_MPI(&mod), mod_int));

    for(i=0; i<256; i++) {
        ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_add_mod(
            UPGP_MPI(&acc),
            UPGP_MPI(&acc),
            UPGP_MPI(&term),
            UPGP_MPI(&mod)
            ));

        acc_int = (acc_int + term_int)%mod_int;

        ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_copy_uint32(UPGP_MPI(&exp), acc_int));
        ck_assert_int_eq(upgp_mpi_compare(UPGP_MPI(&acc), UPGP_MPI(&exp)), 0);
    }
}
END_TEST

START_TEST(test_mpi_add_mod_different_short_dst)
{
    upgp_mpi32_t terma = {4, {75,45,0,0}};
    upgp_mpi32_t termb = {4, {45,75,0,0}};
    upgp_mpi32_t dst = {3, {0,0,0,0}};
    upgp_mpi32_t mod = {4, {0,0,0,0}};

    ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_copy_uint32(UPGP_MPI(&mod), 0x00ffffff));

    ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_add_mod(
        UPGP_MPI(&dst),
        UPGP_MPI(&terma),
        UPGP_MPI(&termb),
        UPGP_MPI(&mod)
        ));

    ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_copy_uint32(UPGP_MPI(&mod), 0xffffffff));

    /* Overflow if mod doesn't fit in destination */
    ck_assert_int_eq(UPGP_MPI_ERROR_OVERFLOW, upgp_mpi_add_mod(
        UPGP_MPI(&dst),
        UPGP_MPI(&terma),
        UPGP_MPI(&termb),
        UPGP_MPI(&mod)
        ));
}
END_TEST

START_TEST(test_mpi_mul_mod_same_size)
{
    upgp_mpi32_t exp_mpi = {4, {0,0,0,0}};
    upgp_mpi32_t prod_mpi = {4, {0,0,0,0}};
    upgp_mpi32_t facta_mpi = {4, {0,0,0,0}};
    upgp_mpi32_t factb_mpi = {4, {0,0,0,0}};
    upgp_mpi32_t mod_mpi = {4, {0,0,0,0}};

    uint64_t facta;
    uint64_t factb;
    uint64_t mod;

    mod = 0x9712ab32;
    upgp_mpi_copy_uint32(UPGP_MPI(&mod_mpi), mod);

    facta = 0x54b3c401;
    factb = 0x36ad6d8a;

    upgp_mpi_copy_uint32(UPGP_MPI(&facta_mpi), facta);
    upgp_mpi_copy_uint32(UPGP_MPI(&factb_mpi), factb);
    upgp_mpi_copy_uint32(UPGP_MPI(&exp_mpi), (facta*factb)%mod);
    ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_mul_mod(UPGP_MPI(&prod_mpi), UPGP_MPI(&facta_mpi), UPGP_MPI(&factb_mpi), UPGP_MPI(&mod_mpi)));
    ck_assert_int_eq(upgp_mpi_compare(UPGP_MPI(&prod_mpi), UPGP_MPI(&exp_mpi)), 0);

    facta = 0x36ce94f3;
    factb = 0x7c86f583;

    upgp_mpi_copy_uint32(UPGP_MPI(&facta_mpi), facta);
    upgp_mpi_copy_uint32(UPGP_MPI(&factb_mpi), factb);
    upgp_mpi_copy_uint32(UPGP_MPI(&exp_mpi), (facta*factb)%mod);
    ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_mul_mod(UPGP_MPI(&prod_mpi), UPGP_MPI(&facta_mpi), UPGP_MPI(&factb_mpi), UPGP_MPI(&mod_mpi)));
    ck_assert_int_eq(upgp_mpi_compare(UPGP_MPI(&prod_mpi), UPGP_MPI(&exp_mpi)), 0);

    facta = 0x41f2da44;
    factb = 0x87cbcfae;

    upgp_mpi_copy_uint32(UPGP_MPI(&facta_mpi), facta);
    upgp_mpi_copy_uint32(UPGP_MPI(&factb_mpi), factb);
    upgp_mpi_copy_uint32(UPGP_MPI(&exp_mpi), (facta*factb)%mod);
    ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_mul_mod(UPGP_MPI(&prod_mpi), UPGP_MPI(&facta_mpi), UPGP_MPI(&factb_mpi), UPGP_MPI(&mod_mpi)));
    ck_assert_int_eq(upgp_mpi_compare(UPGP_MPI(&prod_mpi), UPGP_MPI(&exp_mpi)), 0);

    facta = 0x447e0db6;
    factb = 0x75f23db2;

    upgp_mpi_copy_uint32(UPGP_MPI(&facta_mpi), facta);
    upgp_mpi_copy_uint32(UPGP_MPI(&factb_mpi), factb);
    upgp_mpi_copy_uint32(UPGP_MPI(&exp_mpi), (facta*factb)%mod);
    ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_mul_mod(UPGP_MPI(&prod_mpi), UPGP_MPI(&facta_mpi), UPGP_MPI(&factb_mpi), UPGP_MPI(&mod_mpi)));
    ck_assert_int_eq(upgp_mpi_compare(UPGP_MPI(&prod_mpi), UPGP_MPI(&exp_mpi)), 0);

    facta = 0x80d33f5b;
    factb = 0xe68a506;

    upgp_mpi_copy_uint32(UPGP_MPI(&facta_mpi), facta);
    upgp_mpi_copy_uint32(UPGP_MPI(&factb_mpi), factb);
    upgp_mpi_copy_uint32(UPGP_MPI(&exp_mpi), (facta*factb)%mod);
    ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_mul_mod(UPGP_MPI(&prod_mpi), UPGP_MPI(&facta_mpi), UPGP_MPI(&factb_mpi), UPGP_MPI(&mod_mpi)));
    ck_assert_int_eq(upgp_mpi_compare(UPGP_MPI(&prod_mpi), UPGP_MPI(&exp_mpi)), 0);

    facta = 0x754178e8;
    factb = 0x4fcaf7b8;

    upgp_mpi_copy_uint32(UPGP_MPI(&facta_mpi), facta);
    upgp_mpi_copy_uint32(UPGP_MPI(&factb_mpi), factb);
    upgp_mpi_copy_uint32(UPGP_MPI(&exp_mpi), (facta*factb)%mod);
    ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_mul_mod(UPGP_MPI(&prod_mpi), UPGP_MPI(&facta_mpi), UPGP_MPI(&factb_mpi), UPGP_MPI(&mod_mpi)));
    ck_assert_int_eq(upgp_mpi_compare(UPGP_MPI(&prod_mpi), UPGP_MPI(&exp_mpi)), 0);

    facta = 0x76c6c60c;
    factb = 0x82e47494;

    upgp_mpi_copy_uint32(UPGP_MPI(&facta_mpi), facta);
    upgp_mpi_copy_uint32(UPGP_MPI(&factb_mpi), factb);
    upgp_mpi_copy_uint32(UPGP_MPI(&exp_mpi), (facta*factb)%mod);
    ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_mul_mod(UPGP_MPI(&prod_mpi), UPGP_MPI(&facta_mpi), UPGP_MPI(&factb_mpi), UPGP_MPI(&mod_mpi)));
    ck_assert_int_eq(upgp_mpi_compare(UPGP_MPI(&prod_mpi), UPGP_MPI(&exp_mpi)), 0);

    facta = 0x64ec6433;
    factb = 0xdf8d58d;

    upgp_mpi_copy_uint32(UPGP_MPI(&facta_mpi), facta);
    upgp_mpi_copy_uint32(UPGP_MPI(&factb_mpi), factb);
    upgp_mpi_copy_uint32(UPGP_MPI(&exp_mpi), (facta*factb)%mod);
    ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_mul_mod(UPGP_MPI(&prod_mpi), UPGP_MPI(&facta_mpi), UPGP_MPI(&factb_mpi), UPGP_MPI(&mod_mpi)));
    ck_assert_int_eq(upgp_mpi_compare(UPGP_MPI(&prod_mpi), UPGP_MPI(&exp_mpi)), 0);

    facta = 0xb4ea0f;
    factb = 0xea2a45a;

    upgp_mpi_copy_uint32(UPGP_MPI(&facta_mpi), facta);
    upgp_mpi_copy_uint32(UPGP_MPI(&factb_mpi), factb);
    upgp_mpi_copy_uint32(UPGP_MPI(&exp_mpi), (facta*factb)%mod);
    ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_mul_mod(UPGP_MPI(&prod_mpi), UPGP_MPI(&facta_mpi), UPGP_MPI(&factb_mpi), UPGP_MPI(&mod_mpi)));
    ck_assert_int_eq(upgp_mpi_compare(UPGP_MPI(&prod_mpi), UPGP_MPI(&exp_mpi)), 0);

    facta = 0x18913bbc;
    factb = 0x906ca7e5;

    upgp_mpi_copy_uint32(UPGP_MPI(&facta_mpi), facta);
    upgp_mpi_copy_uint32(UPGP_MPI(&factb_mpi), factb);
    upgp_mpi_copy_uint32(UPGP_MPI(&exp_mpi), (facta*factb)%mod);
    ck_assert_int_eq(UPGP_MPI_SUCCESS, upgp_mpi_mul_mod(UPGP_MPI(&prod_mpi), UPGP_MPI(&facta_mpi), UPGP_MPI(&factb_mpi), UPGP_MPI(&mod_mpi)));
    ck_assert_int_eq(upgp_mpi_compare(UPGP_MPI(&prod_mpi), UPGP_MPI(&exp_mpi)), 0);
}
END_TEST

int main(void)
{
    Suite *s = suite_create("Multi precision integers");
    TCase *tc;
    SRunner *sr = srunner_create(s);
    int nf;

    tc = tcase_create("Assignments");
    suite_add_tcase(s, tc);
    tcase_add_test(tc, test_mpi_copy_same_size);
    tcase_add_test(tc, test_mpi_copy_size_increase);
    tcase_add_test(tc, test_mpi_copy_size_decrease_fit);
    tcase_add_test(tc, test_mpi_copy_size_decrease_overflow);

    tc = tcase_create("Assignments-uint32");
    suite_add_tcase(s, tc);
    tcase_add_test(tc, test_mpi_copy_uint32_same_size);
    tcase_add_test(tc, test_mpi_copy_uint32_size_increase);
    tcase_add_test(tc, test_mpi_copy_uint32_size_decrease_fit);
    tcase_add_test(tc, test_mpi_copy_uint32_size_decrease_overflow);

    tc = tcase_create("Comparisons");
    suite_add_tcase(s, tc);
    tcase_add_test(tc, test_mpi_compare_same_size);
    tcase_add_test(tc, test_mpi_compare_long_hi);
    tcase_add_test(tc, test_mpi_compare_long_low);

    tc = tcase_create("Add/Mod");
    suite_add_tcase(s, tc);
    tcase_add_test(tc, test_mpi_add_mod_same_size);
    tcase_add_test(tc, test_mpi_add_mod_different_term_sizes);
    tcase_add_test(tc, test_mpi_add_mod_different_short_dst);

    tc = tcase_create("Mul/Mod");
    suite_add_tcase(s, tc);
    tcase_add_test(tc, test_mpi_mul_mod_same_size);

    srunner_run_all(sr, CK_ENV);
    nf = srunner_ntests_failed(sr);
    srunner_free(sr);

    return nf == 0 ? 0 : 1;
}
