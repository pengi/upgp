#include <check.h>

#include <upgp/core/buffer.h>
#include <upgp/crypto/mpi.h>
#include <upgp/toolkit/mpi_dec.h>

#include <stdio.h>

START_TEST(test_mpi_dec_parse)
{
    upgp_mpi_dec_storage_t mpidec;
    upgp_mpi64_t dst = {8, {9,9,9,9,9,9,9,9}};
    upgp_mpi64_t exp = {8, {3,2,1,0,0,0,0,0}};
    UPGP_BUFFER(buf, 16);
    /* 17 bits, value 0x010203 */
    upgp_buffer_append(&buf, (const uint8_t[]){0,17,1,2,3}, 5);

    upgp_mpi_dec_init(&mpidec);

    ck_assert_int_eq(UPGP_STREAM_STATUS_DONE, upgp_mpi_dec_parse(&mpidec, UPGP_MPI(&dst), &buf));
    ck_assert_int_eq(0, memcmp(&dst, &exp, sizeof(upgp_mpi64_t)));

    ck_assert_int_eq(0, upgp_buffer_size(&buf));
}
END_TEST

START_TEST(test_mpi_dec_parse_too_big)
{
    upgp_mpi_dec_storage_t mpidec;
    upgp_mpi64_t dst = {8, {9,9,9,9,9,9,9,9}};
    UPGP_BUFFER(buf, 16);
    /* 17 bits, value 0x010203 */
    upgp_buffer_append(&buf, (const uint8_t[]){0,66,2,0,0,0,0,0,0,0,0}, 11);

    upgp_mpi_dec_init(&mpidec);

    ck_assert_int_eq(UPGP_STREAM_STATUS_ERROR, upgp_mpi_dec_parse(&mpidec, UPGP_MPI(&dst), &buf));
    ck_assert_int_eq(0, upgp_buffer_size(&buf));
}
END_TEST

int main(void)
{
    Suite *s = suite_create("Multi precision integers parsing");
    TCase *tc;
    SRunner *sr = srunner_create(s);
    int nf;

    tc = tcase_create("mpi-parse");
    suite_add_tcase(s, tc);
    tcase_add_test(tc, test_mpi_dec_parse);
    tcase_add_test(tc, test_mpi_dec_parse_too_big);

    srunner_run_all(sr, CK_ENV);
    nf = srunner_ntests_failed(sr);
    srunner_free(sr);

    return nf == 0 ? 0 : 1;
}
