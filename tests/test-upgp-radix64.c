#include <check.h>

#include <upgp/toolkit/radix64.h>

START_TEST(test_decode_length)
{
	uint8_t out[3];
	ck_assert_int_eq(3, upgp_radix64_decode_block(out, (const uint8_t *)"AAAA"));
	ck_assert_int_eq(2, upgp_radix64_decode_block(out, (const uint8_t *)"AAA="));
	ck_assert_int_eq(1, upgp_radix64_decode_block(out, (const uint8_t *)"AA=="));
	ck_assert_int_eq(-1, upgp_radix64_decode_block(out, (const uint8_t *)"A===")); /* error */
}
END_TEST

START_TEST(test_decode_dont_overflow)
{
	uint8_t out[4] = {0x55, 0x55, 0x55, 0x55};
	ck_assert_int_eq(3, upgp_radix64_decode_block(out, (const uint8_t *)"AAAA"));
	ck_assert_int_eq(0x00, out[0]);
	ck_assert_int_eq(0x00, out[1]);
	ck_assert_int_eq(0x00, out[2]);
	ck_assert_int_eq(0x55, out[3]); /* Should not be touched */

	out[3] = 0xAA;
	ck_assert_int_eq(3, upgp_radix64_decode_block(out, (const uint8_t *)"AAAA"));
	ck_assert_int_eq(0xAA, out[3]); /* Should not be touched */

}
END_TEST

START_TEST(test_decode_chars)
{
	uint8_t out[3];
	uint8_t in[4] = {0, 'A', '=', '='};
	char chars[65] = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/";
	int i;
	for(i=0;i<64;i++) {
		in[0] = chars[i];
		ck_assert_int_eq(1, upgp_radix64_decode_block(out, in));
		ck_assert_int_eq(i<<2, out[0]);
	}
}
END_TEST

START_TEST(test_decode_block_placement)
{
	uint8_t out[3];
	ck_assert_int_eq(3, upgp_radix64_decode_block(out, (const uint8_t *)"/wAA"));
	ck_assert_int_eq(0xFF, out[0]);
	ck_assert_int_eq(0x00, out[1]);
	ck_assert_int_eq(0x00, out[2]);
	ck_assert_int_eq(3, upgp_radix64_decode_block(out, (const uint8_t *)"AP8A"));
	ck_assert_int_eq(0x00, out[0]);
	ck_assert_int_eq(0xFF, out[1]);
	ck_assert_int_eq(0x00, out[2]);
	ck_assert_int_eq(3, upgp_radix64_decode_block(out, (const uint8_t *)"AAD/"));
	ck_assert_int_eq(0x00, out[0]);
	ck_assert_int_eq(0x00, out[1]);
	ck_assert_int_eq(0xFF, out[2]);
	ck_assert_int_eq(3, upgp_radix64_decode_block(out, (const uint8_t *)"qlWq"));
	ck_assert_int_eq(0xAA, out[0]);
	ck_assert_int_eq(0x55, out[1]);
	ck_assert_int_eq(0xAA, out[2]);
}
END_TEST

START_TEST(test_crc_empty)
{
	uint32_t crc;
	upgp_radix64_crc_init(&crc);
	upgp_radix64_crc_finish(&crc);
	ck_assert_int_eq(0xb704cel, crc);
}
END_TEST

START_TEST(test_crc_apply_empty)
{
	uint32_t crc;
	upgp_radix64_crc_init(&crc);
	upgp_radix64_crc_apply(&crc, (uint8_t *)"", 0);
	upgp_radix64_crc_finish(&crc);
	ck_assert_int_eq(0xb704cel, crc);
}
END_TEST

START_TEST(test_crc_message)
{
	uint32_t crc;
	upgp_radix64_crc_init(&crc);
	upgp_radix64_crc_apply(&crc, (uint8_t *)"badboll", 5);
	upgp_radix64_crc_finish(&crc);
	ck_assert_int_eq(0x1e8209l, crc);
}
END_TEST

int main(void)
{
    Suite *s = suite_create("Radix64");
    TCase *tc;
    SRunner *sr = srunner_create(s);
    int nf;

    tc = tcase_create("Radix64 decode");
    suite_add_tcase(s, tc);
    tcase_add_test(tc, test_decode_length);
    tcase_add_test(tc, test_decode_dont_overflow);
    tcase_add_test(tc, test_decode_chars);
    tcase_add_test(tc, test_decode_block_placement);

    tc = tcase_create("Radix64 CRC");
    suite_add_tcase(s, tc);
    tcase_add_test(tc, test_crc_empty);
    tcase_add_test(tc, test_crc_apply_empty);
    tcase_add_test(tc, test_crc_message);

    srunner_run_all(sr, CK_ENV);
    nf = srunner_ntests_failed(sr);
    srunner_free(sr);

    return nf == 0 ? 0 : 1;
}
