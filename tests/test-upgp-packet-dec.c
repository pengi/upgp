#include <check.h>

#include <upgp/upgp.h>
#include <stdio.h>

UPGP_BUFFER(inbuf, 64);
UPGP_BUFFER(outbuf, 64);

static upgp_stream_status_t byte_counter_stream_data(uint32_t *counter, upgp_buffer_t *buffer);
static const upgp_stream_def_t byte_counter_stream = {
        .data = UPGP_STREAM_DATA_CALLBACK(byte_counter_stream_data) };

static uint32_t size;
static upgp_packet_dec_context_t pp;
static upgp_packet_dec_output_handler_t packet_handlers[] = {
    {1, UPGP_STREAM_CONN(byte_counter_stream, &size, &outbuf)},
    {0, {NULL, NULL, NULL}}
};
static upgp_stream_conn_t instream = UPGP_STREAM_CONN(upgp_packet_dec_input_stream, &pp, &inbuf);

static upgp_stream_status_t assert_consume_bytes(upgp_stream_conn_t *stream, int16_t size) {
    uint8_t tmp_buf[256] = {0};
    upgp_stream_status_t status;
    while(size > 0) {
        int16_t cur_size = 256;
        if(cur_size > size) {
            cur_size = size;
        }
        status = upgp_stream_send(stream, tmp_buf, cur_size);
        size -= cur_size;
        ck_assert_int_eq(UPGP_STREAM_STATUS_MORE, status);
    }
    status = upgp_stream_flush(stream);
    ck_assert_int_eq(0, upgp_buffer_size(stream->buffer));
    return status;
}

START_TEST(test_packet_dec_old_one_byte)
{
    upgp_stream_status_t status;
    upgp_buffer_clear(&inbuf);
    upgp_buffer_clear(&outbuf);
    upgp_packet_dec_context_init(&pp, packet_handlers);

    size = 0;
    status = upgp_stream_send(&instream, (const uint8_t []){0x84, 0x03}, 2);
    ck_assert_int_eq(UPGP_STREAM_STATUS_MORE, status);
    status = assert_consume_bytes(&instream, 3);
    ck_assert_int_eq(UPGP_STREAM_STATUS_MORE, status);
    ck_assert_int_eq(3, size);

    upgp_packet_dec_context_finish(&pp);
}
END_TEST

START_TEST(test_packet_dec_old_two_byte)
{
    upgp_stream_status_t status;
    upgp_buffer_clear(&inbuf);
    upgp_buffer_clear(&outbuf);
    upgp_packet_dec_context_init(&pp, packet_handlers);

    size = 0;
    status = upgp_stream_send(&instream, (const uint8_t []){0x85, 0x00, 0x03}, 3);
    ck_assert_int_eq(UPGP_STREAM_STATUS_MORE, status);
    status = assert_consume_bytes(&instream, 3);
    ck_assert_int_eq(UPGP_STREAM_STATUS_MORE, status);
    ck_assert_int_eq(3, size);

    upgp_packet_dec_context_finish(&pp);
}
END_TEST

START_TEST(test_packet_dec_old_four_byte)
{
    upgp_stream_status_t status;
    upgp_buffer_clear(&inbuf);
    upgp_buffer_clear(&outbuf);
    upgp_packet_dec_context_init(&pp, packet_handlers);

    size = 0;
    status = upgp_stream_send(&instream, (const uint8_t []){0x86, 0x00, 0x00, 0x00, 0x03}, 5);
    ck_assert_int_eq(UPGP_STREAM_STATUS_MORE, status);
    status = assert_consume_bytes(&instream, 3);
    ck_assert_int_eq(UPGP_STREAM_STATUS_MORE, status);
    ck_assert_int_eq(3, size);

    upgp_packet_dec_context_finish(&pp);
}
END_TEST

START_TEST(test_packet_dec_old_indetermined)
{
    /* FIXME */
}
END_TEST

START_TEST(test_packet_dec_new_one_byte)
{
    upgp_stream_status_t status;
    upgp_buffer_clear(&inbuf);
    upgp_buffer_clear(&outbuf);
    upgp_packet_dec_context_init(&pp, packet_handlers);

    size = 0;
    status = upgp_stream_send(&instream, (const uint8_t []){0xc1, 0}, 2);
    ck_assert_int_eq(UPGP_STREAM_STATUS_MORE, status);
    status = assert_consume_bytes(&instream, 0);
    ck_assert_int_eq(UPGP_STREAM_STATUS_MORE, status);
    ck_assert_int_eq(0, size);

    size = 0;
    status = upgp_stream_send(&instream, (const uint8_t []){0xc1, 3}, 2);
    ck_assert_int_eq(UPGP_STREAM_STATUS_MORE, status);
    status = assert_consume_bytes(&instream, 3);
    ck_assert_int_eq(UPGP_STREAM_STATUS_MORE, status);
    ck_assert_int_eq(3, size);

    size = 0;
    status = upgp_stream_send(&instream, (const uint8_t []){0xc1, 191}, 2);
    ck_assert_int_eq(UPGP_STREAM_STATUS_MORE, status);
    status = assert_consume_bytes(&instream, 191);
    ck_assert_int_eq(UPGP_STREAM_STATUS_MORE, status);
    ck_assert_int_eq(191, size);

    upgp_packet_dec_context_finish(&pp);
}
END_TEST

START_TEST(test_packet_dec_new_two_byte)
{
    upgp_stream_status_t status;
    upgp_buffer_clear(&inbuf);
    upgp_buffer_clear(&outbuf);
    upgp_packet_dec_context_init(&pp, packet_handlers);

    size = 0;
    status = upgp_stream_send(&instream, (const uint8_t []){0xc1, 192, 0}, 3);
    ck_assert_int_eq(UPGP_STREAM_STATUS_MORE, status);
    status = assert_consume_bytes(&instream, ((192-192)<<8) + 0 + 192);
    ck_assert_int_eq(UPGP_STREAM_STATUS_MORE, status);
    ck_assert_int_eq(((192-192)<<8) + 0 + 192, size);

    size = 0;
    status = upgp_stream_send(&instream, (const uint8_t []){0xc1, 210, 17}, 3);
    ck_assert_int_eq(UPGP_STREAM_STATUS_MORE, status);
    status = assert_consume_bytes(&instream, ((210-192)<<8) + 17 + 192);
    ck_assert_int_eq(UPGP_STREAM_STATUS_MORE, status);
    ck_assert_int_eq(((210-192)<<8) + 17 + 192, size);

    size = 0;
    status = upgp_stream_send(&instream, (const uint8_t []){0xc1, 223, 255}, 3);
    ck_assert_int_eq(UPGP_STREAM_STATUS_MORE, status);
    status = assert_consume_bytes(&instream, ((223-192)<<8) + 255 + 192);
    ck_assert_int_eq(UPGP_STREAM_STATUS_MORE, status);
    ck_assert_int_eq(((223-192)<<8) + 255 + 192, size);
    /* Verify length calculation according to RFC4880 */
    ck_assert_int_eq(8383, ((223-192)<<8) + 255 + 192);

    upgp_packet_dec_context_finish(&pp);
}
END_TEST

START_TEST(test_packet_dec_new_five_byte)
{
    upgp_stream_status_t status;
    upgp_buffer_clear(&inbuf);
    upgp_buffer_clear(&outbuf);
    upgp_packet_dec_context_init(&pp, packet_handlers);

    size = 0;
    status = upgp_stream_send(&instream, (const uint8_t []){0xc1, 255, 0x00, 0x00, 0x00, 0x00}, 6);
    ck_assert_int_eq(UPGP_STREAM_STATUS_MORE, status);
    status = assert_consume_bytes(&instream, 0x00000000);
    ck_assert_int_eq(UPGP_STREAM_STATUS_MORE, status);
    ck_assert_int_eq(0x00000000, size);

    size = 0;
    status = upgp_stream_send(&instream, (const uint8_t []){0xc1, 255, 0x00, 0x00, 0x03, 0x13}, 6);
    ck_assert_int_eq(UPGP_STREAM_STATUS_MORE, status);
    status = assert_consume_bytes(&instream, 0x00000313);
    ck_assert_int_eq(UPGP_STREAM_STATUS_MORE, status);
    ck_assert_int_eq(0x00000313, size);

    upgp_packet_dec_context_finish(&pp);
}
END_TEST

int main(void)
{
    Suite *s = suite_create("Packet decoding");
    TCase *tc;
    SRunner *sr = srunner_create(s);
    int nf;

    tc = tcase_create("Old packet format");
    suite_add_tcase(s, tc);
    tcase_add_test(tc, test_packet_dec_old_one_byte);
    tcase_add_test(tc, test_packet_dec_old_two_byte);
    tcase_add_test(tc, test_packet_dec_old_four_byte);
    tcase_add_test(tc, test_packet_dec_old_indetermined);

    tc = tcase_create("New packet format");
    suite_add_tcase(s, tc);
    tcase_add_test(tc, test_packet_dec_new_one_byte);
    tcase_add_test(tc, test_packet_dec_new_two_byte);
    tcase_add_test(tc, test_packet_dec_new_five_byte);

    srunner_run_all(sr, CK_ENV);
    nf = srunner_ntests_failed(sr);
    srunner_free(sr);

    return nf == 0 ? 0 : 1;
}

static upgp_stream_status_t byte_counter_stream_data(uint32_t *counter, upgp_buffer_t *buffer) {
    *counter += upgp_buffer_size(buffer);
    upgp_buffer_clear(buffer);
    return UPGP_STREAM_STATUS_MORE;
}
