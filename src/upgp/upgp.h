#ifndef UPGP_UPGP_H
#define UPGP_UPGP_H

#include <upgp/core/reent.h>
#include <upgp/core/buffer.h>
#include <upgp/core/stream.h>

#include <upgp/processing/armor_dec.h>
#include <upgp/processing/packet_dec.h>
#include <upgp/processing/public_key_dec.h>
#include <upgp/processing/signature_dec.h>

#endif
