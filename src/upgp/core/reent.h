#ifndef UPGP_REENT_H
#define UPGP_REENT_H

#include <stdlib.h>

typedef struct {
	void *target;
} upgp_reent_t;

#define UPGP_REENT_INIT(_RE) \
	do { \
		(_RE)->target = NULL; \
	} while(0)

#define UPGP_REENT_START(_RE) \
	do { \
		if((_RE)->target != NULL) { \
			goto *(_RE)->target; \
		} \
	} while(0)

#define UPGP_REENT_END(_RE) UPGP_REENT_INIT(_RE)

#define UPGP_REENT_BREAK(_RE, _RETURN) \
	do { \
		__label__ target_label; \
		(_RE)->target = &&target_label; \
		return _RETURN; \
		target_label: \
		0; \
	} while(0)

#endif
