#ifndef UPGP_BUFFER_H
#define UPGP_BUFFER_H

#include <stdint.h>

typedef struct {
	int16_t allocated;
	int16_t start;
	int16_t end;
	uint32_t counter;
	uint8_t *data;
} upgp_buffer_t;

#define UPGP_BUFFER(_NAME, _SIZE) \
	uint8_t _NAME ## _data[(_SIZE)]; \
	upgp_buffer_t _NAME = { \
			.allocated = (_SIZE), \
			.start = 0, \
			.end = 0, \
			.counter = 0, \
			.data = _NAME ## _data \
	}

static inline uint8_t *upgp_buffer_start(upgp_buffer_t *buf) {
	return buf->data + buf->start;
}

static inline uint8_t *upgp_buffer_end(upgp_buffer_t *buf) {
	return buf->data + buf->end;
}

static inline int16_t upgp_buffer_size(upgp_buffer_t *buf) {
    return buf->end - buf->start;
}

static inline uint16_t upgp_buffer_counter(upgp_buffer_t *buf) {
    return buf->counter;
}

static inline int16_t upgp_buffer_avail(upgp_buffer_t *buf) {
	return buf->allocated - buf->end;
}

static inline void upgp_buffer_clear(upgp_buffer_t *buf) {
    buf->start = buf->end = buf->counter = 0;
}

static inline void upgp_buffer_take(upgp_buffer_t *buf, int16_t len) {
	buf->start += len;
	buf->counter += len;
	if(buf->start == buf->end) {
	    buf->start = buf->end = 0;
	}
}


void upgp_buffer_pack(upgp_buffer_t *buffer);

int16_t upgp_buffer_locate(upgp_buffer_t *buffer, int chr);

int16_t upgp_buffer_append(upgp_buffer_t *buffer, const uint8_t *data, int16_t len);


#define UPGP_BUFFER_FILTER(_BUF, _LENGTH, _C, _FILTER) \
	do { \
		int16_t write_ptr; \
		int16_t read_ptr; \
		uint8_t *buf_ptr = upgp_buffer_start(_BUF); \
		write_ptr = read_ptr = (_LENGTH); \
		while(read_ptr > 0) { \
			uint8_t _C; \
			_C = buf_ptr[--read_ptr]; \
			if(_FILTER) { \
				buf_ptr[--write_ptr] = _C; \
			} \
		} \
		upgp_buffer_take(data, write_ptr); \
	} while(0)


#endif
