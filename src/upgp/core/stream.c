#include <upgp/core/stream.h>
#include <stdio.h>

upgp_stream_status_t upgp_stream_send(
		upgp_stream_conn_t *stream,
		const uint8_t *source,
		int16_t len
) {
	while(len > 0) {
		int16_t send_len = upgp_buffer_append(stream->buffer, source, len);
		source += send_len;
		len -= send_len;
		if(upgp_buffer_avail(stream->buffer) == 0) {
			upgp_stream_status_t status;
			status = (*stream->def->data)(stream->storage, stream->buffer);
			if(status != UPGP_STREAM_STATUS_MORE) {
				return status;
			}
			if(upgp_buffer_avail(stream->buffer) == 0) {
				return UPGP_STREAM_STATUS_BUFFER_FULL;
			}
		}
	}
	return UPGP_STREAM_STATUS_MORE;
}


upgp_stream_status_t upgp_stream_flush(
		upgp_stream_conn_t *stream
) {
    int16_t buf_size;
	while((buf_size = upgp_buffer_size(stream->buffer)) > 0) {
		upgp_stream_status_t status;
		status = (*stream->def->data)(stream->storage, stream->buffer);
		if(status != UPGP_STREAM_STATUS_MORE) {
			return status;
		}
        if(buf_size == upgp_buffer_size(stream->buffer)) {
            /* Havn't consumed anything */
            return UPGP_STREAM_STATUS_ERROR;
        }
	}
	return UPGP_STREAM_STATUS_MORE;
}


upgp_stream_status_t upgp_stream_block_start(
        upgp_stream_conn_t *stream
) {
    upgp_stream_status_t status;
    status = upgp_stream_flush(stream);
    if(status != UPGP_STREAM_STATUS_MORE) {
        return status;
    }
    if(stream->def->block_start != NULL) {
        return (*stream->def->block_start)(stream->storage);
    }
    return UPGP_STREAM_STATUS_MORE;
}

upgp_stream_status_t upgp_stream_block_end(
        upgp_stream_conn_t *stream
) {
    upgp_stream_status_t status;
    status = upgp_stream_flush(stream);
    if(status != UPGP_STREAM_STATUS_MORE) {
        return status;
    }
    if(stream->def->block_end != NULL) {
        return (*stream->def->block_end)(stream->storage);
    }
    return UPGP_STREAM_STATUS_MORE;
}
