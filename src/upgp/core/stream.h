#ifndef UPGP_STREAM_H
#define UPGP_STREAM_H

#include <stdint.h>
#include <upgp/core/buffer.h>

typedef enum {
	UPGP_STREAM_STATUS_MORE = 0,
	UPGP_STREAM_STATUS_DONE,
	UPGP_STREAM_STATUS_ERROR,
	UPGP_STREAM_STATUS_BUFFER_FULL
} upgp_stream_status_t;

typedef struct {
    upgp_stream_status_t (*data)(void *storage, upgp_buffer_t *buffer);
    upgp_stream_status_t (*block_start)(void *storage);
    upgp_stream_status_t (*block_end)(void *storage);
} upgp_stream_def_t;

typedef struct {
	const upgp_stream_def_t *def;
	void *storage;
	upgp_buffer_t *buffer;
} upgp_stream_conn_t;

#define UPGP_STREAM_DATA_CALLBACK(_CALLBACK) \
        (upgp_stream_status_t (*)(void *, upgp_buffer_t *))(_CALLBACK)

#define UPGP_STREAM_EVENT_CALLBACK(_CALLBACK) \
        (upgp_stream_status_t (*)(void *))(_CALLBACK)

#define UPGP_STREAM_CONN(_DEF, _STORAGE, _BUFFER) \
		(upgp_stream_conn_t){&_DEF, (void*)(_STORAGE), (_BUFFER)}


/*
 * Reentrant stream processing methods
 */

#define UPGP_STREAM_START(_REENT) \
		upgp_reent_t *reent = (_REENT); \
		UPGP_REENT_START(reent);

#define UPGP_STREAM_END \
		UPGP_REENT_END(reent); \
		return UPGP_STREAM_STATUS_DONE;

#define UPGP_STREAM_BREAK_UNTIL(_COND) \
		do { \
			while(!(_COND)) { \
				upgp_buffer_pack(data); \
				if(upgp_buffer_avail(data) == 0) { \
					return UPGP_STREAM_STATUS_BUFFER_FULL; \
				} \
				UPGP_REENT_BREAK(reent, UPGP_STREAM_STATUS_MORE); \
			} \
		} while(0)

#define UPGP_STREAM_ASSERT(_COND) \
		do { \
			if(!(_COND)) { \
				UPGP_REENT_INIT(reent); \
				return UPGP_STREAM_STATUS_ERROR; \
			} \
		} while(0)

#define UPGP_STREAM_ERROR \
        UPGP_STREAM_ASSERT(0)

#define UPGP_STREAM_PARSE(_METHOD) \
    do { \
        while(1) { \
            upgp_stream_status_t _status = (_METHOD); \
            if(_status == UPGP_STREAM_STATUS_DONE) { \
                break; \
            } \
            upgp_buffer_pack(data); \
            UPGP_REENT_BREAK(reent, _status); \
        } \
    } while(0)

/*
 * Stream output
 */

upgp_stream_status_t upgp_stream_send(
		upgp_stream_conn_t *stream,
		const uint8_t *source,
		int16_t len
);

upgp_stream_status_t upgp_stream_flush(
		upgp_stream_conn_t *stream
);

upgp_stream_status_t upgp_stream_block_start(
        upgp_stream_conn_t *stream
);

upgp_stream_status_t upgp_stream_block_end(
        upgp_stream_conn_t *stream
);

#define UPGP_STREAM_BLOCK_START(_OUT) \
        do { \
            upgp_stream_status_t status = upgp_stream_block_start(_OUT); \
            if(status != UPGP_STREAM_STATUS_MORE) { \
                UPGP_REENT_END(reent); \
                return status; \
            } \
        } while(0)

#define UPGP_STREAM_BLOCK_END(_OUT) \
        do { \
            upgp_stream_status_t status = upgp_stream_block_end(_OUT); \
            if(status != UPGP_STREAM_STATUS_MORE) { \
                UPGP_REENT_END(reent); \
                return status; \
            } \
        } while(0)

#define UPGP_STREAM_SEND(_OUT, _DATA, _LEN) \
		do { \
			upgp_stream_status_t status = upgp_stream_send((_OUT), (_DATA), (_LEN)); \
			if(status == UPGP_STREAM_STATUS_ERROR || status == UPGP_STREAM_STATUS_BUFFER_FULL) { \
				UPGP_REENT_END(reent); \
				return status; \
			} \
		} while(0)


#endif
