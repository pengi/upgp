#include <upgp/core/buffer.h>
#include <string.h>

void upgp_buffer_pack(upgp_buffer_t *buffer) {
	if(buffer->start == 0) {
		return;
	}
	memcpy(buffer->data, upgp_buffer_start(buffer), upgp_buffer_size(buffer));
	buffer->end = upgp_buffer_size(buffer);
	buffer->start = 0;
}

int16_t upgp_buffer_locate(upgp_buffer_t *buffer, int chr) {
	uint8_t *pos = memchr(upgp_buffer_start(buffer), chr, upgp_buffer_size(buffer));
	if(pos == NULL) {
		return -1;
	}
	return pos - upgp_buffer_start(buffer);
}

int16_t upgp_buffer_append(upgp_buffer_t *buffer, const uint8_t *data, int16_t len) {
	if(len > upgp_buffer_avail(buffer)) {
		len = upgp_buffer_avail(buffer);
	}
	memcpy(upgp_buffer_end(buffer), data, len);
	buffer->end += len;
	return len;
}
