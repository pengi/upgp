#ifndef UPGP_RSA_H
#define UPGP_RSA_H

#include <upgp/crypto/mpi.h>

typedef struct {
    uint16_t num;
    upgp_mpi32_t e;
    upgp_mpi4096_t n;
} upgp_rsa_t;

#define UPGP_RSA_INIT { \
    .num = 2, \
    .e = UPGP_MPI32_INIT, \
    .n = UPGP_MPI4096_INIT, \
}

#endif
