#include <upgp/crypto/mpi.h>
#include <string.h>
#include <stdio.h>

static inline int mem_is_zero(
    const uint8_t *ptr,
    int size)
{
    while (size--)
    {
        if (*(ptr++) != 0)
        {
            return 0;
        }
    }
    return 1;
}

static inline int upgp_mpi_fits(const upgp_mpi_t *val, int size) {
    if(size >= val->len) {
        return 1;
    }
    return mem_is_zero(val->data + size, val->len - size);
}

upgp_mpi_status_t upgp_mpi_copy(
    upgp_mpi_t *dst,
    const upgp_mpi_t *src)
{
    if (dst->len < src->len)
    {
        int i;

        memcpy(dst->data, src->data, dst->len);

        if (!upgp_mpi_fits(src, dst->len))
        {
            return UPGP_MPI_ERROR_OVERFLOW;
        }

        /* check if truncated bits are zero */
        return UPGP_MPI_SUCCESS;
    }
    memset(dst->data + src->len, 0, dst->len - src->len);
    memcpy(dst->data, src->data, src->len);
    return UPGP_MPI_SUCCESS;
}

upgp_mpi_status_t upgp_mpi_copy_uint32(
    upgp_mpi_t *dst,
    uint32_t src)
{
    upgp_mpi32_t src_mpi;
    src_mpi.len = 4;
    src_mpi.data[0] = (src>> 0)&0xff;
    src_mpi.data[1] = (src>> 8)&0xff;
    src_mpi.data[2] = (src>>16)&0xff;
    src_mpi.data[3] = (src>>24)&0xff;
    return upgp_mpi_copy(dst, UPGP_MPI(&src_mpi));
}

upgp_mpi_status_t upgp_mpi_zero(
    upgp_mpi_t *dst)
{
    memset(dst->data, 0, dst->len);
    return UPGP_MPI_SUCCESS;
}

int upgp_mpi_compare(
    const upgp_mpi_t *s1,
    const upgp_mpi_t *s2)
{
    if(!upgp_mpi_fits(s1, s2->len)) {
        return 1;
    }
    if(!upgp_mpi_fits(s2, s1->len)) {
        return -1;
    }
    return memcmp(s1->data, s2->data, (s1->len < s2->len) ? s1->len : s2->len);
}

upgp_mpi_status_t upgp_mpi_add_mod(
    upgp_mpi_t *dst,
    const upgp_mpi_t *terma,
    const upgp_mpi_t *termb,
    const upgp_mpi_t *mod)
{
    int wrap_around = 0;
    int i;
    uint16_t sum;

    /* Check for wrap-around */
    sum = 0;
    for(i=0; i<terma->len || i<termb->len || i<mod->len; i++) {
        int diff = 0;
        if(i < terma->len) {
            sum += terma->data[i];
        }
        if(i < termb->len) {
            sum += termb->data[i];
        }
        if(i < mod->len) {
            diff = (int)(sum&0xff) - (int)mod->data[i];
        } else {
            diff = (int)(sum&0xff);
        }
        if(diff != 0) {
            wrap_around = (diff>0);
        }
        sum >>= 8;
    }
    if(sum > 0) {
        wrap_around = 1;
    }

    /* Do addition */
    sum = 0;
    for(i=0; i<dst->len; i++) {
        if(i < terma->len) {
            sum += terma->data[i];
        }
        if(i < termb->len) {
            sum += termb->data[i];
        }
        if(wrap_around && i<mod->len) {
            sum -= mod->data[i];
        }
        dst->data[i] = sum&0xff;
        sum = (sum>>8) | (sum & 0x8000 ? 0xff00 : 0x0000);
    }

    return upgp_mpi_fits(mod, dst->len) ? UPGP_MPI_SUCCESS : UPGP_MPI_ERROR_OVERFLOW;
}

upgp_mpi_status_t upgp_mpi_mul_mod(
    upgp_mpi_t *prod,
    upgp_mpi_t *facta,
    const upgp_mpi_t *factb,
    const upgp_mpi_t *mod)
{
    int bitnum;
    /* Set accumulator to zero */
    upgp_mpi_zero(prod);

    for(bitnum = 0; bitnum < factb->len*8; bitnum++) {
        if(factb->data[bitnum/8] & (1<<(bitnum%8))) {
            upgp_mpi_add_mod(prod,prod,facta,mod);
        }
        upgp_mpi_add_mod(facta,facta,facta,mod);
    }
    return UPGP_MPI_SUCCESS;
}
