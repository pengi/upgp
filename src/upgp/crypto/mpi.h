#ifndef UPGP_MPI_H
#define UPGP_MPI_H

#include <stdint.h>

typedef enum {
    UPGP_MPI_SUCCESS = 0,
    UPGP_MPI_ERROR_OVERFLOW
} upgp_mpi_status_t;

typedef struct {
    uint16_t len; /* Actual size of array, in bytes */
    uint8_t data[];
} upgp_mpi_t;

#define UPGP_MPI_SIZED_TYPE(_SIZE) \
    typedef struct { \
        uint16_t len; \
        uint8_t data[(_SIZE + 7)/8]; \
    } upgp_mpi ## _SIZE ## _t

UPGP_MPI_SIZED_TYPE(32);
UPGP_MPI_SIZED_TYPE(64);
UPGP_MPI_SIZED_TYPE(2048);
UPGP_MPI_SIZED_TYPE(4096);

#define UPGP_MPI_INIT(_N) {(_N)/8, {0}}
#define UPGP_MPI32_INIT   UPGP_MPI_INIT(32)
#define UPGP_MPI64_INIT   UPGP_MPI_INIT(64)
#define UPGP_MPI2048_INIT UPGP_MPI_INIT(2048)
#define UPGP_MPI4096_INIT UPGP_MPI_INIT(4096)
#define UPGP_MPI(_V)      ((upgp_mpi_t *)(_V))

upgp_mpi_status_t upgp_mpi_copy(upgp_mpi_t *dst, const upgp_mpi_t *src);
upgp_mpi_status_t upgp_mpi_copy_uint32(upgp_mpi_t *dst, uint32_t src);
upgp_mpi_status_t upgp_mpi_zero(upgp_mpi_t *dst);
int upgp_mpi_compare(const upgp_mpi_t *s1, const upgp_mpi_t *s2);

upgp_mpi_status_t upgp_mpi_add_mod(
    upgp_mpi_t *dst,
    const upgp_mpi_t *terma,
    const upgp_mpi_t *termb,
    const upgp_mpi_t *mod);

/* facta will be destroyed */
upgp_mpi_status_t upgp_mpi_mul_mod(
    upgp_mpi_t *prod,
    upgp_mpi_t *facta,
    const upgp_mpi_t *factb,
    const upgp_mpi_t *mod);

#endif
