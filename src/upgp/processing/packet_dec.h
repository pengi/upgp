#ifndef UPGP_PROCESSING_PACKET_DEC_H
#define UPGP_PROCESSING_PACKET_DEC_H

#include <stdint.h>
#include <upgp/core/stream.h>
#include <upgp/core/reent.h>

typedef struct {
    uint8_t tag;
    upgp_stream_conn_t output_stream;
} upgp_packet_dec_output_handler_t;

typedef struct {
    upgp_packet_dec_output_handler_t *output;
    upgp_stream_conn_t *cur_output;
    upgp_reent_t reent;

    uint32_t length;
    uint8_t partial;
    uint8_t tag;
} upgp_packet_dec_context_t;

void upgp_packet_dec_context_init(upgp_packet_dec_context_t *context, upgp_packet_dec_output_handler_t *output);
void upgp_packet_dec_context_finish(upgp_packet_dec_context_t *context);

extern const upgp_stream_def_t upgp_packet_dec_input_stream;

#endif
