#include <upgp/processing/signature_dec.h>
#include <upgp/toolkit/stddec.h>
#include <stdio.h>
#include <string.h>

static upgp_stream_status_t upgp_signature_dec_input_data(
		upgp_signature_dec_context_t *context, upgp_buffer_t *data);
static upgp_stream_status_t upgp_signature_dec_input_block_start(
		upgp_signature_dec_context_t *context);
static upgp_stream_status_t upgp_signature_dec_input_block_end(
		upgp_signature_dec_context_t *context);

const upgp_stream_def_t upgp_signature_dec_input_stream = {
		.data = UPGP_STREAM_DATA_CALLBACK(upgp_signature_dec_input_data),
		.block_start = UPGP_STREAM_EVENT_CALLBACK(
				upgp_signature_dec_input_block_start),
		.block_end = UPGP_STREAM_EVENT_CALLBACK(
				upgp_signature_dec_input_block_end) };

void upgp_signature_dec_context_init(upgp_signature_dec_context_t *context) {
	UPGP_REENT_INIT(&context->reent);
	context->sign.len = 4096/8;
}

void upgp_signature_dec_context_finish(upgp_signature_dec_context_t *context) {
}

static upgp_stream_status_t upgp_signature_dec_input_block_start(
		upgp_signature_dec_context_t *context) {
	UPGP_REENT_INIT(&context->reent);
	return UPGP_STREAM_STATUS_MORE;
}
static upgp_stream_status_t upgp_signature_dec_input_block_end(
		upgp_signature_dec_context_t *context) {
	return UPGP_STREAM_STATUS_MORE;
}

static upgp_stream_status_t upgp_signature_dec_input_data(
		upgp_signature_dec_context_t *context, upgp_buffer_t *data) {
    uint32_t val;
	UPGP_STREAM_START(&context->reent)
    uint8_t *ptr;
	int i;

    /* Version */
    UPGP_STREAM_PARSE(upgp_uint8_to_32_parse(&val, data));

    printf("Signature packet version %u\n", val);

    if (val == 3) {
    	/* Version 3 - not implemented yet */
        UPGP_STREAM_ERROR;
    } else if (val == 4) {
    	/* Version 4*/
        UPGP_STREAM_PARSE(upgp_uint8_parse(&context->type, data));
        UPGP_STREAM_PARSE(upgp_uint8_parse(&context->pk_algo, data));
        UPGP_STREAM_PARSE(upgp_uint8_parse(&context->hash_algo, data));

        printf("signature type %3d pk algo %3d hash algo %3d\n",
            context->type, context->pk_algo, context->hash_algo);

        /* Size of hashed subpackets */
        UPGP_STREAM_PARSE(upgp_uint16_to_32_parse(&val, data));
        context->block_end = upgp_buffer_counter(data) + val;

        while(upgp_buffer_counter(data) - context->block_end != 0) {
            /* subpacket size */
            UPGP_STREAM_PARSE(upgp_size4_parse(&val, 0, data));
            context->packet_end = upgp_buffer_counter(data) + val;
            printf("    Hashed subpacket size %3d ", val-1);

            /* Type */
            UPGP_STREAM_PARSE(upgp_uint8_to_32_parse(&val, data));
            printf("type %3d:", val);

            while(upgp_buffer_counter(data) - context->packet_end != 0) {
                UPGP_STREAM_BREAK_UNTIL(upgp_buffer_size(data) >= 1);
                printf(" %02x", *upgp_buffer_start(data));
                upgp_buffer_take(data, 1);
            }
            printf("\n");
        }

        /* Size of unhashed subpackets */
        UPGP_STREAM_PARSE(upgp_uint16_to_32_parse(&val, data));
        context->block_end = upgp_buffer_counter(data) + val;

        while(upgp_buffer_counter(data) - context->block_end != 0) {
            /* subpacket size */
            UPGP_STREAM_PARSE(upgp_size4_parse(&val, 0, data));
            context->packet_end = upgp_buffer_counter(data) + val;
            printf("Non-hashed subpacket size %3d ", val-1);

            /* Type */
            UPGP_STREAM_PARSE(upgp_uint8_to_32_parse(&val, data));
            printf("type %3d:", val);

            while(upgp_buffer_counter(data) - context->packet_end != 0) {
                UPGP_STREAM_BREAK_UNTIL(upgp_buffer_size(data) >= 1);
                printf(" %02x", *upgp_buffer_start(data));
                upgp_buffer_take(data, 1);
            }
            printf("\n");
        }

        UPGP_STREAM_PARSE(upgp_uint16_to_32_parse(&val, data));
        context->block_end = upgp_buffer_counter(data) + val;

        printf("Signed hash value %04x\n", val);

        upgp_mpi_dec_init(&context->sign_dec);
        UPGP_STREAM_PARSE(upgp_mpi_dec_parse(&context->sign_dec, UPGP_MPI(&context->sign), data));

        printf("sign =");
        for (i = 0; i < context->sign.len; i++)
        {
            printf(" %02x", context->sign.data[context->sign.len - i - 1]);
        }
        printf("\n");
    } else {
    	/* Unknown verison */
        UPGP_STREAM_ERROR;
    }


    while(1) {
        UPGP_STREAM_BREAK_UNTIL(upgp_buffer_size(data) >= 1);
        upgp_buffer_take(data, 1);
    }

	UPGP_STREAM_END
}
