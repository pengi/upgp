#ifndef UPGP_PROCESSING_PUBLIC_KEY_DEC_H
#define UPGP_PROCESSING_PUBLIC_KEY_DEC_H

#include <upgp/core/stream.h>
#include <upgp/toolkit/mpi_dec.h>
#include <upgp/crypto/rsa.h>

typedef struct {
    upgp_reent_t reent;
    upgp_mpi_dec_storage_t mpidec;
    upgp_rsa_t rsa;
} upgp_public_key_dec_context_t;

void upgp_public_key_dec_context_init(upgp_public_key_dec_context_t *context);
void upgp_public_key_dec_context_finish(upgp_public_key_dec_context_t *context);

extern const upgp_stream_def_t upgp_public_key_dec_input_stream;


#endif
