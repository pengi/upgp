#ifndef UPGP_SIGNATURE_DEC_H
#define UPGP_SIGNATURE_DEC_H

#include <stdint.h>
#include <upgp/core/stream.h>
#include <upgp/core/reent.h>
#include <upgp/crypto/mpi.h>
#include <upgp/toolkit/mpi_dec.h>

#define UPGP_ARMOR_MAX_LINE_LENGTH 128

typedef struct {
	upgp_reent_t reent;

	uint8_t type;
	uint8_t pk_algo;
	uint8_t hash_algo;
    uint32_t block_end;
    uint32_t packet_end;

	upgp_mpi_dec_storage_t sign_dec;
	upgp_mpi4096_t sign;
} upgp_signature_dec_context_t;

void upgp_signature_dec_context_init(upgp_signature_dec_context_t *context);
void upgp_signature_dec_context_finish(upgp_signature_dec_context_t *context);

extern const upgp_stream_def_t upgp_signature_dec_input_stream;

#endif
