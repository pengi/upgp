#ifndef UPGP_ARMOR_H
#define UPGP_ARMOR_H

#include <stdint.h>
#include <upgp/core/stream.h>
#include <upgp/core/reent.h>

#define UPGP_ARMOR_MAX_LINE_LENGTH 128

typedef struct {
	upgp_stream_conn_t output_stream;
	upgp_reent_t reent;
	uint32_t crc;
} upgp_armor_dec_context_t;

void upgp_armor_dec_context_init(upgp_armor_dec_context_t *context, upgp_stream_conn_t output_stream);
void upgp_armor_dec_context_finish(upgp_armor_dec_context_t *context);

extern const upgp_stream_def_t upgp_armor_dec_input_stream;

#endif
