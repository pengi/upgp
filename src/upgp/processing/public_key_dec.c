#include <upgp/processing/public_key_dec.h>
#include <upgp/toolkit/mpi_dec.h>
#include <upgp/toolkit/stddec.h>
#include <stdio.h>

static upgp_stream_status_t upgp_public_key_dec_input_block_start(
    upgp_public_key_dec_context_t *context);
static upgp_stream_status_t upgp_public_key_dec_input_block_end(
    upgp_public_key_dec_context_t *context);
static upgp_stream_status_t upgp_public_key_dec_input_data(
    upgp_public_key_dec_context_t *context,
    upgp_buffer_t *data);

const upgp_stream_def_t upgp_public_key_dec_input_stream =
{
    .data = UPGP_STREAM_DATA_CALLBACK(upgp_public_key_dec_input_data),
    .block_start = UPGP_STREAM_EVENT_CALLBACK(upgp_public_key_dec_input_block_start),
    .block_end = UPGP_STREAM_EVENT_CALLBACK(upgp_public_key_dec_input_block_end)
};

void upgp_public_key_dec_context_init(
    upgp_public_key_dec_context_t *context)
{
    UPGP_REENT_INIT(&context->reent);
    context->rsa = (upgp_rsa_t)UPGP_RSA_INIT;
}

void upgp_public_key_dec_context_finish(
    upgp_public_key_dec_context_t *context)
{

}

upgp_stream_status_t upgp_public_key_dec_input_block_start(
    upgp_public_key_dec_context_t *context)
{
    printf("New block\n");
    UPGP_REENT_INIT(&context->reent);
    return UPGP_STREAM_STATUS_MORE;
}

upgp_stream_status_t upgp_public_key_dec_input_block_end(
    upgp_public_key_dec_context_t *context)
{
    printf("Block end\n");
    return UPGP_STREAM_STATUS_MORE;
}

static upgp_stream_status_t upgp_public_key_dec_input_data(
    upgp_public_key_dec_context_t *context,
    upgp_buffer_t *data)
{
    uint32_t val;
    int i;
    UPGP_STREAM_START(&context->reent)

    upgp_mpi_dec_init(&context->mpidec);

    /* Version */
    UPGP_STREAM_PARSE(upgp_uint8_to_32_parse(&val, data));

    if (val == 3)
    {
        /* Version 3 - Not implemented yet */
        UPGP_STREAM_ERROR;
    }
    else if (val == 4)
    {
        /* Version 4 public key */

        /* Time */
        UPGP_STREAM_PARSE(upgp_uint32_parse(&val, data));

        /* Algorithm */
        UPGP_STREAM_PARSE(upgp_uint8_to_32_parse(&val, data));

        /* assert RSA = 1 */
        UPGP_STREAM_ASSERT(val == 1);

        printf("Decoding RSA\n");
        /* Modulo */
        UPGP_STREAM_PARSE(
            upgp_mpi_dec_parse(&context->mpidec, UPGP_MPI(&context->rsa.n), data));
        /* Exponent */
        UPGP_STREAM_PARSE(
            upgp_mpi_dec_parse(&context->mpidec, UPGP_MPI(&context->rsa.e), data));


        printf("N =");
        for (i = 0; i < context->rsa.n.len; i++)
        {
            printf(" %02x", context->rsa.n.data[context->rsa.n.len - i - 1]);
        }
        printf("\n");
        printf("e =");
        for (i = 0; i < context->rsa.e.len; i++)
        {
            printf(" %02x", context->rsa.e.data[context->rsa.e.len - i - 1]);
        }
        printf("\n");

        printf("Done with RSA\n");
    }
    else
    {
        /* Unknown version */
        UPGP_STREAM_ERROR;
    }

    while(1) {
        UPGP_STREAM_BREAK_UNTIL(upgp_buffer_size(data) >= 1);
        upgp_buffer_take(data, 1);
    }

    UPGP_STREAM_END
}
