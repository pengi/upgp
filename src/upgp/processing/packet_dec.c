#include <upgp/core/reent.h>
#include <upgp/core/stream.h>
#include <upgp/processing/packet_dec.h>
#include <upgp/toolkit/stddec.h>
#include <stdio.h>

static upgp_stream_status_t upgp_packet_dec_input_data(
    upgp_packet_dec_context_t *context,
    upgp_buffer_t *data);

const upgp_stream_def_t upgp_packet_dec_input_stream =
{ .data = UPGP_STREAM_DATA_CALLBACK(upgp_packet_dec_input_data), .block_start =
NULL, .block_end = NULL };

void upgp_packet_dec_context_init(
    upgp_packet_dec_context_t *context,
    upgp_packet_dec_output_handler_t *output)
{
    context->output = output;
    UPGP_REENT_INIT(&context->reent);
}
void upgp_packet_dec_context_finish(
    upgp_packet_dec_context_t *context)
{

}

static upgp_stream_status_t upgp_packet_dec_input_data(
    upgp_packet_dec_context_t *context,
    upgp_buffer_t *data)
{
    uint8_t *buf;
    int i;
    UPGP_STREAM_START(&context->reent)

    while (1)
    {
        /* Read packet tag byte */
        UPGP_STREAM_PARSE(upgp_uint8_parse(&context->tag, data));

        UPGP_STREAM_ASSERT(context->tag & 0x80);

        context->partial = 0;
        /* Read size */
        if (context->tag & 0x40)
        {
            /* New format packet */
            context->tag &= 0x3f;
            context->length = 0;

            UPGP_STREAM_PARSE(upgp_size4_parse(&context->length, 1, data));
            /* TODO: partial packets, now treated as error */
        }
        else
        {
            /* Old format packet */
            switch (context->tag & 0x03)
            {
            case 0:
                UPGP_STREAM_PARSE(upgp_uint8_to_32_parse(&context->length, data));
                break;
            case 1:
                UPGP_STREAM_PARSE(upgp_uint16_to_32_parse(&context->length, data));
                break;
            case 2:
                UPGP_STREAM_PARSE(upgp_uint32_parse(&context->length, data));
                break;
            case 3:
                UPGP_STREAM_ASSERT(0); /* FIXME: Not implemented */
                break;
            }
            context->tag &= 0x3c;
            context->tag >>= 2;
        }

        /* Locate handler */
        context->cur_output = NULL;
        for (i = 0; context->output[i].tag != 0; i++)
        {
            if (context->output[i].tag == context->tag)
            {
                context->cur_output = &context->output[i].output_stream;
            }
        }

        if (context->cur_output != NULL)
        {
            /* Ignore unknown messages */
            UPGP_STREAM_BLOCK_START(context->cur_output);
        }
        while (context->length)
        {
            UPGP_STREAM_BREAK_UNTIL(upgp_buffer_size(data) > 0);

            int16_t cur_size = upgp_buffer_size(data);
            if (cur_size > context->length)
            {
                cur_size = context->length;
            }

            if (context->cur_output != NULL)
            {
                /* Ignore unknown messages */
                UPGP_STREAM_SEND(context->cur_output, upgp_buffer_start(data),
                    cur_size);
            }

            context->length -= cur_size;
            upgp_buffer_take(data, cur_size);
        }
        if (context->cur_output != NULL)
        {
            /* Ignore unknown messages */
            UPGP_STREAM_BLOCK_END(context->cur_output);
        }
    }

    UPGP_STREAM_END
}
