#include <upgp/core/reent.h>
#include <upgp/core/stream.h>
#include <upgp/processing/armor_dec.h>
#include <upgp/toolkit/radix64.h>
#include <stdio.h>
#include <string.h>

static upgp_stream_status_t upgp_armor_dec_input_data(upgp_armor_dec_context_t *context,
		upgp_buffer_t *data);

const upgp_stream_def_t upgp_armor_dec_input_stream = {
    .data = UPGP_STREAM_DATA_CALLBACK(upgp_armor_dec_input_data),
    .block_start = NULL,
    .block_end = NULL
};

void upgp_armor_dec_context_init(upgp_armor_dec_context_t *context,
		upgp_stream_conn_t output_stream) {
	context->output_stream = output_stream;
	UPGP_REENT_INIT(&context->reent);
}

void upgp_armor_dec_context_finish(upgp_armor_dec_context_t *context) {
}

static upgp_stream_status_t upgp_armor_dec_input_data(upgp_armor_dec_context_t *context,
		upgp_buffer_t *data) {
	int16_t full_len, len;
	int line_ok = 0;
	char *line_start;

	UPGP_STREAM_START(&context->reent)

	/*
	 * Start of block
	 */
	do {
		UPGP_STREAM_BREAK_UNTIL((full_len = upgp_buffer_locate(data, '\n')) >= 0);
		line_start = (char*)upgp_buffer_start(data);
		len = full_len;
		while(*line_start == ' ' || *line_start == '\r') {
			line_start++;
			len--;
		}
		while(len > 0 && (line_start[len-1] == ' ' || line_start[len-1] == '\r')) {
			len--;
		}

		line_ok = 1;
		if(len < 16) {
			line_ok = 0;
		} else if(0 != memcmp(line_start, "-----BEGIN ", 11)) {
			line_ok = 0;
		} else if(0 != memcmp(line_start+len-5, "-----", 5)) {
			line_ok = 0;
		}

		if(line_ok) {
			line_start[len-5] = '\0';
			/* TODO: Handler of block type here */
		}
		upgp_buffer_take(data, full_len+1);
	} while(!line_ok);

	upgp_stream_block_start(&context->output_stream);

	/*
	 * Header lines
	 */
	do {
		UPGP_STREAM_BREAK_UNTIL((full_len = upgp_buffer_locate(data, '\n')) >= 0);
		line_start = (char*)upgp_buffer_start(data);
		len = full_len;
		while(*line_start == ' ' || *line_start == '\r') {
			line_start++;
			len--;
		}
		while(len > 0 && (line_start[len-1] == ' ' || line_start[len-1] == '\r')) {
			len--;
		}
		line_start[len] = '\0';
		if(len > 0) {
		    /* TODO: Header line can be processed here */
		}
		upgp_buffer_take(data, full_len+1);
	} while(len > 0);

	upgp_radix64_crc_init(&context->crc);

	/*
	 * Data lines
	 */
	do {
		UPGP_STREAM_BREAK_UNTIL(upgp_buffer_size(data) >= 4);
		/* Strip whitespaces chars */
		UPGP_BUFFER_FILTER(data, 4, c, c != ' ' && c != '\t' && c != '\n' && c != '\r');


		/* Is next an data-crc-delimiter? */
		if(upgp_buffer_size(data) >= 1) {
			if(*upgp_buffer_start(data) == '=') {
				upgp_buffer_take(data, 1);
				break;
			}
		}

		/* Otherwise, decode and continue */
		if(upgp_buffer_size(data) >= 4) {
			uint8_t *in = upgp_buffer_start(data);
			uint8_t out[3];
			int16_t out_size;

			out_size = upgp_radix64_decode_block(out, in);

			upgp_radix64_crc_apply(&context->crc, out, out_size);

			UPGP_STREAM_SEND(&context->output_stream, out, out_size);
			upgp_buffer_take(data, 4);
		}
	} while(1);

	upgp_stream_flush(&context->output_stream);
	upgp_radix64_crc_finish(&context->crc);

	/*
	 * CRC
	 */
	do {
		UPGP_STREAM_BREAK_UNTIL(upgp_buffer_size(data) >= 4);
		UPGP_BUFFER_FILTER(data, 4, c, c != ' ' && c != '\t' && c != '\n' && c != '\r');
		if(upgp_buffer_size(data) < 4)
			continue;

		/* Is next an data-crc-delimiter? */
		uint8_t *in = upgp_buffer_start(data);
		uint8_t out[3] = {0};
		int16_t out_size;
		uint32_t act_crc;

		out_size = upgp_radix64_decode_block(out, in);
		upgp_buffer_take(data, 4);

		UPGP_STREAM_ASSERT(out_size == 3);

		act_crc = (uint32_t)out[0]<<16 | (uint32_t)out[1]<<8 | (uint32_t)out[2];
		UPGP_STREAM_ASSERT(act_crc == context->crc);
	} while(0);

	/*
	 * Start of block
	 */
	do {
		UPGP_STREAM_BREAK_UNTIL((full_len = upgp_buffer_locate(data, '\n')) >= 0);
		line_start = (char*)upgp_buffer_start(data);
		len = full_len;
		while(*line_start == ' ' || *line_start == '\r') {
			line_start++;
			len--;
		}
		while(len > 0 && (line_start[len-1] == ' ' || line_start[len-1] == '\r')) {
			len--;
		}

		line_ok = 1;
		if(len < 16) {
			line_ok = 0;
		} else if(0 != memcmp(line_start, "-----END ", 9)) {
			line_ok = 0;
		} else if(0 != memcmp(line_start+len-5, "-----", 5)) {
			line_ok = 0;
		}

		if(line_ok) {
			line_start[len-5] = '\0';
            /* TODO: Handler of block type here, verify that it's the same */
		}
		upgp_buffer_take(data, full_len+1);
	} while(!line_ok);

    upgp_stream_block_end(&context->output_stream);


	UPGP_STREAM_END
}
