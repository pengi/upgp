#ifndef UPGP_STDDEC_H
#define UPGP_STDDEC_H

#include <upgp/core/stream.h>

upgp_stream_status_t upgp_uint8_parse(
    uint8_t *dst,
    upgp_buffer_t *data);

upgp_stream_status_t upgp_uint16_parse(
    uint16_t *dst,
    upgp_buffer_t *data);

upgp_stream_status_t upgp_uint32_parse(
    uint32_t *dst,
    upgp_buffer_t *data);

/*
 * Helpers to parse shorter integers to longer variables
 */
static inline upgp_stream_status_t upgp_uint8_to_16_parse(
    uint16_t *dst,
    upgp_buffer_t *data)
{
    uint8_t tmp;
    upgp_stream_status_t status;
    status = upgp_uint8_parse(&tmp, data);
    *dst = tmp;
    return status;
}

static inline upgp_stream_status_t upgp_uint8_to_32_parse(
    uint32_t *dst,
    upgp_buffer_t *data)
{
    uint8_t tmp;
    upgp_stream_status_t status;
    status = upgp_uint8_parse(&tmp, data);
    *dst = tmp;
    return status;
}

static inline upgp_stream_status_t upgp_uint16_to_32_parse(
    uint32_t *dst,
    upgp_buffer_t *data)
{
    uint16_t tmp;
    upgp_stream_status_t status;
    status = upgp_uint16_parse(&tmp, data);
    *dst = tmp;
    return status;
}

/*
 * Decode size byte as of OpenPGP version 4 formats.
 *
 * Both used for packet sizes, and packet sizes in signatures
 */

upgp_stream_status_t upgp_size4_parse(
    uint32_t *dst,
    uint8_t allow_partial,
    upgp_buffer_t *data);

#endif
