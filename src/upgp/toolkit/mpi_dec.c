#include <upgp/toolkit/mpi_dec.h>

void upgp_mpi_dec_init(
    upgp_mpi_dec_storage_t *storage)
{
    UPGP_REENT_INIT(&storage->reent);
}

upgp_stream_status_t upgp_mpi_dec_parse(
    upgp_mpi_dec_storage_t *storage,
    upgp_mpi_t *dst,
    upgp_buffer_t *data)
{
    uint8_t *ptr;
    UPGP_STREAM_START(&storage->reent)

    /* Init storage */
    storage->write_pos = dst->len;
    storage->data_left = 0;

    /* Parse header */
    UPGP_STREAM_BREAK_UNTIL(upgp_buffer_size(data) >= 2);
    ptr = upgp_buffer_start(data);
    storage->data_left = ((uint16_t)ptr[0])<<8 | ((uint16_t)ptr[1]);
    upgp_buffer_take(data, 2);

    /* Convert bit count to byte count */
    storage->data_left = (storage->data_left+7)/8;

    /* Will it fit? */
    if(dst->len < storage->data_left) {
        while(storage->data_left) {
            UPGP_STREAM_BREAK_UNTIL(upgp_buffer_size(data) >= 1);
            upgp_buffer_take(data, 1);
            storage->data_left--;
        }
        UPGP_STREAM_ERROR;
    }

    /* Fill mpi with padding */
    while(storage->write_pos > storage->data_left) {
        dst->data[--storage->write_pos] = 0;
    }

    /* Fill with data */
    while(storage->data_left) {
        UPGP_STREAM_BREAK_UNTIL(upgp_buffer_size(data) >= 1);
        dst->data[--storage->write_pos] = *upgp_buffer_start(data);
        upgp_buffer_take(data, 1);
        storage->data_left--;
    }

    /* Sanity check, should always be true */
    UPGP_STREAM_ASSERT(storage->write_pos == 0);

    UPGP_STREAM_END
}
