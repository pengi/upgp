#ifndef UPGP_RADIX64_H
#define UPGP_RADIX64_H

#include <stdint.h>

int16_t upgp_radix64_decode_block(uint8_t *dst, const uint8_t *src);

void upgp_radix64_crc_init(uint32_t *crc);
void upgp_radix64_crc_apply(uint32_t *crc, const uint8_t *data, int16_t length);
void upgp_radix64_crc_finish(uint32_t *crc);

#endif
