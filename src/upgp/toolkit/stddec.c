#include <upgp/toolkit/stddec.h>

upgp_stream_status_t upgp_uint8_parse(
    uint8_t *dst,
    upgp_buffer_t *data)
{
    const uint8_t *ptr;
    if (upgp_buffer_size(data) < 1)
    {
        return UPGP_STREAM_STATUS_MORE;
    }
    ptr = upgp_buffer_start(data);
    *dst = ptr[0];
    upgp_buffer_take(data, 1);
    return UPGP_STREAM_STATUS_DONE;
}

upgp_stream_status_t upgp_uint16_parse(
    uint16_t *dst,
    upgp_buffer_t *data)
{
    const uint8_t *ptr;
    if (upgp_buffer_size(data) < 2)
    {
        return UPGP_STREAM_STATUS_MORE;
    }
    ptr = upgp_buffer_start(data);
    *dst = ((uint16_t) ptr[0]) << 8 | ((uint32_t) ptr[1]);
    upgp_buffer_take(data, 2);
    return UPGP_STREAM_STATUS_DONE;
}

upgp_stream_status_t upgp_uint32_parse(
    uint32_t *dst,
    upgp_buffer_t *data)
{
    const uint8_t *ptr;
    if (upgp_buffer_size(data) < 4)
    {
        return UPGP_STREAM_STATUS_MORE;
    }
    ptr = upgp_buffer_start(data);
    *dst = ((uint32_t) ptr[0]) << 24 | ((uint32_t) ptr[1]) << 16
        | ((uint32_t) ptr[2]) << 8 | ((uint32_t) ptr[3]);
    upgp_buffer_take(data, 4);
    return UPGP_STREAM_STATUS_DONE;
}

upgp_stream_status_t upgp_size4_parse(
    uint32_t *dst,
    uint8_t allow_partial,
    upgp_buffer_t *data)
{
    const uint8_t *ptr;
    *dst = 0;

    if (upgp_buffer_size(data) < 1)
    {
        return UPGP_STREAM_STATUS_MORE;
    }

    ptr = upgp_buffer_start(data);
    if (ptr[0] <= 191)
    {
        *dst = ptr[0];
        upgp_buffer_take(data, 1);
    }
    else if (ptr[0] <= 223 || (allow_partial && ptr[0] < 255))
    {
        if (upgp_buffer_size(data) < 2)
        {
            return UPGP_STREAM_STATUS_MORE;
        }

        *dst = (uint32_t) (ptr[0] - 192) << 8;
        *dst |= (uint32_t) (ptr[1]);
        *dst += 192;
        upgp_buffer_take(data, 2);
    }
    else if (allow_partial && ptr[0] <= 254)
    {
        /* Partial block */
        /* Partial blocks not supported yet */
        return UPGP_STREAM_STATUS_ERROR;
    }
    else
    {
        if (upgp_buffer_size(data) < 5)
        {
            return UPGP_STREAM_STATUS_MORE;
        }

        *dst = (uint32_t) (ptr[1]) << 24;
        *dst |= (uint32_t) (ptr[2]) << 16;
        *dst |= (uint32_t) (ptr[3]) << 8;
        *dst |= (uint32_t) (ptr[4]);
        upgp_buffer_take(data, 5);
    }

    return UPGP_STREAM_STATUS_DONE;
}
