#ifndef UPGP_MPI_DEC_H
#define UPGP_MPI_DEC_H

#include <upgp/crypto/mpi.h>
#include <upgp/core/stream.h>
#include <upgp/core/reent.h>

typedef struct
{
    upgp_reent_t reent;
    uint16_t write_pos; /*< Last written position, counting down */
    uint16_t data_left;
} upgp_mpi_dec_storage_t;

void upgp_mpi_dec_init(
    upgp_mpi_dec_storage_t *storage);

upgp_stream_status_t upgp_mpi_dec_parse(
    upgp_mpi_dec_storage_t *storage,
    upgp_mpi_t *dst,
    upgp_buffer_t *buf);

#endif
