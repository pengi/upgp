#include <upgp/upgp.h>
#include <stdio.h>
#include <sys/uio.h>
#include <unistd.h>

UPGP_BUFFER(readbuf, 80);
UPGP_BUFFER(bufa, 8);
UPGP_BUFFER(writebuf, 32);

static upgp_stream_status_t out_stream_data(
    void *storage,
    upgp_buffer_t *buffer);
static upgp_stream_status_t out_stream_block_start(
    void *storage);
static upgp_stream_status_t out_stream_block_end(
    void *storage);
static const upgp_stream_def_t out_stream =
{ .data = UPGP_STREAM_DATA_CALLBACK(out_stream_data), .block_start =
    out_stream_block_start, .block_end = out_stream_block_end };

int main(
    int argc,
    char *argv[])
{
    ssize_t read_size;

    upgp_armor_dec_context_t ap;
    upgp_packet_dec_context_t pp;
    upgp_signature_dec_context_t sig;
    upgp_public_key_dec_context_t pubk;
    upgp_stream_status_t status;

    upgp_packet_dec_output_handler_t packet_handlers[] =
    {
    { 2, UPGP_STREAM_CONN(upgp_signature_dec_input_stream, &sig, &writebuf) }, /* Signature */
    { 6, UPGP_STREAM_CONN(upgp_public_key_dec_input_stream, &pubk, &writebuf) }, /* Public key */
    { 13, UPGP_STREAM_CONN(out_stream, NULL, &writebuf) }, /* User ID */
        { 14, UPGP_STREAM_CONN(upgp_public_key_dec_input_stream, &pubk,
            &writebuf) }, /* Public sub-key */
        { 0,
        { NULL, NULL, NULL } } };

    upgp_public_key_dec_context_init(&pubk);
    upgp_packet_dec_context_init(&pp, packet_handlers);
    upgp_signature_dec_context_init(&sig);
    upgp_armor_dec_context_init(&ap,
        UPGP_STREAM_CONN(upgp_packet_dec_input_stream, &pp, &bufa));

    while ((read_size = read(STDIN_FILENO, upgp_buffer_end(&readbuf),
        upgp_buffer_avail(&readbuf))) > 0)
    {
        readbuf.end += read_size;
        status = upgp_armor_dec_input_stream.data(&ap, &readbuf);
        if (status != UPGP_STREAM_STATUS_MORE)
        {
            break;
        }
    }

    upgp_armor_dec_context_finish(&ap);
    upgp_packet_dec_context_finish(&pp);

    printf("Status: %d\n", status);

    return 0;
}

static upgp_stream_status_t out_stream_data(
    void *storage,
    upgp_buffer_t *buffer)
{
    char *ptr;
    int len;
    printf("hex:");
    len = upgp_buffer_size(buffer);
    ptr = (char *) upgp_buffer_start(buffer);
    while (len--)
    {
        printf(" %02x", (uint8_t) *(ptr++));
    }
    printf("\n");

    printf("raw: ");
    len = upgp_buffer_size(buffer);
    ptr = (char *) upgp_buffer_start(buffer);
    while (len--)
    {
        char c = (char) *(ptr++);
        if (c < 0x20 || c >= 0x7f)
        {
            printf(".");
        }
        else
        {
            printf("%c", c);
        }
    }
    printf("\n");
    upgp_buffer_take(buffer, upgp_buffer_size(buffer));
    return UPGP_STREAM_STATUS_MORE;
}

static upgp_stream_status_t out_stream_block_start(
    void *storage)
{
    printf("Block start\n");
    return UPGP_STREAM_STATUS_MORE;
}
static upgp_stream_status_t out_stream_block_end(
    void *storage)
{
    printf("Block end\n");
    return UPGP_STREAM_STATUS_MORE;
}
